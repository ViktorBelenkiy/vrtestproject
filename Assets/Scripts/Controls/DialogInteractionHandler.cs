﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class DialogInteractionHandler : InteractionHandler
{ 
    public Color DefaultColor;
    public Color TransitionColor;

    private DialogBase _dialogBase;
    private int _optionIndex;

    private RectTransform _rTransform;
    private Image _image;

    private void Start()
    {
        base.Init();
        _rTransform = GetComponent<RectTransform>();
        _image = GetComponent<Image>();
        _image.color = DefaultColor;
    }

    public void Init(DialogBase dialogBase, int optionIndex)
    {
        _dialogBase = dialogBase;
        _optionIndex = optionIndex;
    }

    protected override void DoOnPointerEnter()
    {
        _image.color = TransitionColor;
    }

    protected override void DoOnPointerExit()
    {
        _image.color = DefaultColor;
    }

    protected override void Interact()
    {
        IsInFocus = false;
        DoOnPointerExit();
        _dialogBase.ReadOption(_optionIndex);
    }
}
