﻿using UnityEngine;
using UnityEngine.UI;

public class ButtonClickHandler : InteractionHandler
{
    private Button _button;

    private void Start()
    {
        base.Init();
        _button = GetComponent<Button>();
    }

    protected override void Interact()
    {
        if (_button != null)
        {
            _button.onClick.Invoke();
            IsInFocus = false;
        }
    }
}
