﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class PlayerControls : MonoBehaviour {

    public Action Interact { get; private set; }
    public Transform cameraTransform;
    public AudioSource Source;
    private bool _isWalking;

    [SerializeField]
    private CharacterController _characterController;

    public float MoveSpeed;
    public float RotationSpeed;
    public float JumpSpeed;

    private float _gravity = -9.81f;

    private void Start()
    {
        if (_characterController == null)
        {
            Debug.LogError("Character controller is null!");
        }
    }

    public void AddInteractionListener(Action action)
    {
        Interact += action;
    }

    public void RemoveInteractionListener(Action action)
    {
        Interact -= action;
    }
   
	void Update ()
    {
        Vector3 movement = new Vector3();

        var xMovement = Input.GetAxis("ControllerLeftY");
        var yMovement = Input.GetAxis("ControllerLeftX");
        movement = cameraTransform.forward * xMovement * MoveSpeed * Time.deltaTime; 
        movement += cameraTransform.right * yMovement * MoveSpeed * Time.deltaTime; 

        if (Math.Abs(xMovement) > 0 || Math.Abs(yMovement) > 0)
        {
            if (!_isWalking)
            {
                _isWalking = true;
                Source.time = 16f;
                Source.Play();
            }
            
        }
        else
        {
            _isWalking = false;
            Source.Stop();
        }

        if (!_characterController.isGrounded)
        {
            movement.y = _gravity;
        }

        transform.Rotate(Vector3.up * Input.GetAxis("ControllerRightX") * RotationSpeed * Time.deltaTime);

        if (Input.GetButtonDown("Submit"))
        {
            Interact();
        }

        _characterController.Move(movement);
    }
}
