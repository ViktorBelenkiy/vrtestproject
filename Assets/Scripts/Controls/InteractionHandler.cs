﻿using UnityEngine;
using UnityEngine.EventSystems;

public abstract class InteractionHandler : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    protected bool IsInFocus;

    protected abstract void Interact();

    void Start()
    {
        Init();
    }

    protected virtual void Init()
    {
        var player = GameObject.Find("Player").GetComponent<PlayerControls>();
        if (player == null)
        {
            Debug.LogError("Player not found from: " + gameObject.name);
            return;
        }

        player.AddInteractionListener(OnInteract);
    }

    public void OnInteract()
    {
        if (IsInFocus && gameObject.activeInHierarchy)
        {
            Interact();
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        IsInFocus = true;
        DoOnPointerEnter();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        IsInFocus = false;
        DoOnPointerExit();
    }

    protected virtual void DoOnPointerEnter() { }

    protected virtual void DoOnPointerExit() { }
}
