﻿using System.Collections;
using UnityEngine;

public class MainGui : MonoBehaviour
{
    public static MainGui Instance;
    public GameObject DialogBoxPrefab;
    private static GameObject _dialogBoxInstance;
    private static DialogBox _dialogBox;
    private bool _isDialogBoxBusy;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        _dialogBoxInstance = Instantiate(DialogBoxPrefab, gameObject.transform) as GameObject;
        _dialogBox = _dialogBoxInstance.GetComponent<DialogBox>();
        _dialogBoxInstance.SetActive(false);
    }

    public static void StartDialog(DialogBase dialogBase , Dialogue[] dialog)
    {
        if (!Instance._isDialogBoxBusy)
        {
            Instance.StartCoroutine(Instance.PlayDialog(dialog, dialogBase));
        }
    }

    private IEnumerator PlayDialog(Dialogue[] dialog, DialogBase dialogBase)
    {
        _isDialogBoxBusy = true;
        _dialogBoxInstance.SetActive(true);

        dialogBase.gameObject.SetActive(false);

        foreach (Dialogue dialogueLine in dialog)
        {
            _dialogBox.Text.text = string.Empty;
            _dialogBox.CharacterName.text = dialogueLine.CharacterName;

            foreach (char character in dialogueLine.Phrase)
            {
                _dialogBox.Text.text += character;
                yield return new WaitForSeconds(0.015f);
            }

            yield return new WaitForSeconds(Mathf.Clamp(dialogueLine.Phrase.Length/40, 1f, 5f));
        }

        _dialogBoxInstance.SetActive(false);
        dialogBase.SetState(true);
        _isDialogBoxBusy = false;

        yield return null;
    }
}
