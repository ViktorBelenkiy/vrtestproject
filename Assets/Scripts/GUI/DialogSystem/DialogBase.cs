﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class DialogBase : MonoBehaviour
{
    public GameObject OptionsArea;

    public GameObject DialogOptionPrefab;
    private DialogOption[] _dialogOptions;
    private GameObject[] _dialogOptionGameobjects;

    private GameObject _parent;
    private GameObject _player;
    private Transform _camera;
    private RectTransform _dialogTransform;

    private void Start()
    {
        _player = GameObject.Find("Player");
        _camera = _player.transform.GetChild(0).transform;
        _dialogTransform = GetComponent<RectTransform>();
        _dialogTransform.anchoredPosition3D = (_camera.position - transform.position).normalized;
    }

    public void Init(DialogOption[] options, GameObject parent)
    {
        _dialogOptions = options;
        _parent = parent;

        CreateOptions();
        SetState(true);
    }

    private void CreateOptions()
    { 
         _dialogOptionGameobjects = new GameObject[_dialogOptions.Length];
        for (int i = 0; i < _dialogOptions.Length; i++)
        {
            GameObject op = Instantiate(DialogOptionPrefab, OptionsArea.transform) as GameObject;

            _dialogOptions[i].SetDialogBase(this);
            op.GetComponent<DialogInteractionHandler>().Init(this, i);
            op.GetComponentInChildren<Text>().text = _dialogOptions[i].OptionName;
            op.SetActive(false);
            _dialogOptionGameobjects[i] = op;
        }
    }

    private void RefreshOptions()
    {
        Vector2 windowSize = new Vector2(100, 30);

        for (int i = 0; i < _dialogOptions.Length; i++)
        {
            if (!_dialogOptions[i].IsEnabled || _dialogOptions[i].IsDisabledOnRead && _dialogOptions[i].IsRead)
            {
                _dialogOptionGameobjects[i].SetActive(false);
                continue;
            }

            _dialogOptionGameobjects[i].SetActive(true);
            _dialogOptions[i].IsUpdateRequired = false;

            windowSize.y += 20;
        }

        OptionsArea.GetComponent<RectTransform>().sizeDelta = windowSize;
    }

    public void SetState(bool isActive)
    {
        RefreshOptions();

        if (Array.Find(_dialogOptions, p => p.IsEnabled) == null)
        {
            return;
        }

        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(isActive);
        }

        gameObject.SetActive(isActive);
    }

    public void ReadOption(int index)
    {
        _dialogOptions[index].SetRead();
        _dialogOptions[index].FireDialogEvent();
        MainGui.StartDialog(this, _dialogOptions[index].Dialogue);
    }

    private void Update()
    {
        Vector3 v = _camera.position - transform.position;
        v.x = v.z = 0.0f;
        transform.LookAt(_camera.position - v);
        transform.Rotate(0, 180, 0);

        if (Vector3.Distance(transform.position, _camera.position) >= 0.5f ||
            Vector3.Distance(_parent.transform.position, _camera.position) >= 0.5f)
        {
            var test = (_camera.position - _parent.transform.position).normalized;
            test.y = 0.75f;
            _dialogTransform.anchoredPosition3D = test;
        }
    }
}

