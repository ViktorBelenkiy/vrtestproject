﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogBox : MonoBehaviour
{
    public Text CharacterName;
    public Text Text;
    public Image CharacterPortrait;
}
