﻿using System;

[Serializable]
public class DialogOption: IGameEventHandler
{
    public string OptionName;
    public Dialogue[] Dialogue;
    public bool IsEnabled = true;
    public bool IsUpdateRequired = true;
    public bool IsDisabledOnRead;
    public bool IsRead { get; private set; }

    public string EnableEvent;
    public string DisableEvent;
    public string EventToFire;

    private DialogBase _dialogBase;

    public DialogOption()
    {
    }

    public void SetRead()
    {
        IsRead = true;
    }

    public void SetDialogBase(DialogBase dialogBase)
    {
        if (!string.IsNullOrEmpty(EnableEvent) || !string.IsNullOrEmpty(DisableEvent))
        {
            GameEventManager.AddListener(OnGameEvent);
        }
        _dialogBase = dialogBase;
    }

    public void FireDialogEvent()
    {
        if (!string.IsNullOrEmpty(EventToFire))
        {
            GameEventManager.FireEvent(EventToFire);
        }
    }

    public void OnGameEvent(string eventName)
    {
        if (eventName == EnableEvent)
        {
            IsEnabled = true;
            IsUpdateRequired = true;
        }

        if (eventName == DisableEvent)
        {
            IsEnabled = false;
            IsUpdateRequired = false;
        }
    }
}

[Serializable]
public class Dialogue
{
    public string CharacterName;
    public string Phrase;
}

