﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(EventTrigger))]
public class CharacterInteraction : InteractionHandler
{
    public GameObject DialogBase;
    public DialogOption[] DialogOptions;

    private GameObject _dialogInstance;

    private DialogBase _dialogBase;

    private void Start()
    {
        base.Init();

        if (DialogOptions.Length != 0)
        {
            _dialogInstance = Instantiate(DialogBase, gameObject.transform) as GameObject;
            _dialogBase = _dialogInstance.GetComponent<DialogBase>();
            _dialogBase.Init(DialogOptions, gameObject);

            _dialogInstance.SetActive(false);
        }
    }

    protected override void Interact()
    {
        if (_dialogInstance != null && !_dialogInstance.activeInHierarchy)
        {
            _dialogBase.SetState(true);
        }
    }
}
