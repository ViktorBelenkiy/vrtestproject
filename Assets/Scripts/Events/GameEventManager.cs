﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEventManager : MonoBehaviour
{
    public static GameEventManager Instance;

    private Action<string> _fireEvent;

    private void Awake()
    {
        Instance = this;
    }

    public static void AddListener(Action<string> action)
    {
        Instance._fireEvent += action;
    }

    public static void RemoveListener(Action<string> action)
    {
        Instance._fireEvent -= action;
    }

    public static void FireEvent(string eventName)
    {
        Instance._fireEvent(eventName);
    }
}
