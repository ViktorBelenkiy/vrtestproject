﻿using System.Collections;
using System.Collections.Generic;

public interface IGameEventHandler 
{
    void OnGameEvent(string eventName);
}
